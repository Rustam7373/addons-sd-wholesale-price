# Module: wholesale price #

This module allows you to get a wholesale discount for different groups of customers.

### In detail ###

Designed for CMS CS-Cart

### How to install and configure the module? ###

To install, unload the module into the CS-Cart root directory.